/* editor.vala
 *
 * Copyright 2019 Daniel Espinosa Ortiz <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Svgvi.Editor : Gtk.Box {
  private Svgvi.SvgView viewer;
  private Svgvi.SvgSourceEditor source_editor;
  private File _file = null;
  private Cancellable cancellable = null;
  private int _current_row;
  private int _current_column;

  public signal void updated ();
  public signal void loading ();
  public signal void loaded ();

  public File file {
    get {
      return _file;
    }
    set {
      _file = value;
      if (!file.query_exists (cancellable)) {
        warning ("File doesn't exists: %s", _file.get_uri ());
        return;
      }
      loading ();
      load_svg_file.begin (_file);
    }
  }

  public int current_row {
    get { return _current_row; }
    set {
      _current_row = value;
    }
  }
  public int current_column {
    get { return _current_column; }
    set {
      _current_column = value;
    }
  }

  construct {
    var box = new Gtk.Paned (Gtk.Orientation.VERTICAL);
    var sw1 = new Gtk.ScrolledWindow ();
    viewer = new Svgvi.SvgView ();
    box.set_start_child (sw1);
    box.set_end_child (viewer);
    source_editor = new Svgvi.SvgSourceEditor ();
    source_editor.can_focus = true;
    sw1.set_child (source_editor);
    sw1.hexpand = true;
    sw1.vexpand = true;
    source_editor.vexpand = true;
    source_editor.hexpand = true;
    source_editor.enable_search ();
    viewer.vexpand = true;
    viewer.hexpand = true;
    append (box);
    hexpand = true;
    vexpand = true;
    can_focus = true;

    source_editor.source_view.buffer.end_user_action.connect (()=>{
      try {
        if (file == null) {
          warning ("Invalid file");
          return;
        }
        var stream = file.replace (null, false, FileCreateFlags.NONE, null);
        var ostream = new DataOutputStream (stream);
        ostream.put_string (source_editor.source_view.buffer.text, null);
        ostream.close ();
        var doc = new GSvg.Document ();
        doc.read_from_string (source_editor.source_view.buffer.text);
        viewer.svg = doc;
        updated ();
      } catch (GLib.Error e) {
        warning ("Document parsing error: %s", e.message);
      }
    });
    source_editor.source_view.buffer.insert_text.connect ((ref pos, new_text, new_text_length)=>{
      _current_row = pos.get_line ();
      _current_column = pos.get_line_offset ();
    });
  }
  public void save_to (File f) {
    try {
      ((GXml.Document) viewer.svg).write_file (f);
      file = f;
    } catch (GLib.Error e) {
      warning ("Error while trying to saving as, SVG file: %s", e.message);
    }
  }
  public void enable_search () { source_editor.enable_search (); }
  public void disable_search () { source_editor.disable_search (); }

  public void toggle_search () {
    if (source_editor.search_active) {
      disable_search ();
    } else {
      enable_search ();
    }
  }
  public async void load_svg_file (File f) {
    try {
      var istream = _file.read ();
      var ostream = new MemoryOutputStream.resizable ();
      Idle.add(load_svg_file.callback);
      yield;
      ostream.splice (istream, OutputStreamSpliceFlags.CLOSE_SOURCE, cancellable);
      Idle.add(load_svg_file.callback);
      yield;
      source_editor.source_view.buffer.text = (string) ostream.get_data ();
      var doc = new GSvg.Document ();
      Idle.add(load_svg_file.callback);
      yield;
      doc.read_from_string ((string) ostream.get_data ());
      Idle.add(load_svg_file.callback);
      yield;
      new Thread<int> ("svg: "+f.get_basename (), ()=>{
        viewer.svg = doc;
        loaded ();
        return 0;
      });
    } catch (GLib.Error e) {
      warning ("Error parsing SVG source File: %s", e.message);
    }
  }
  public void write_string (string text) {
    source_editor.source_view.buffer.insert_at_cursor (text, text.length);
  }
  public void write_circle () {
    GSvg.AnimatedLength cx, cy, r;
    cx = new GSvg.AnimatedLength ();
    cx.value = "0mm";
    cy = new GSvg.AnimatedLength ();
    cy.value = "0mm";
    r = new GSvg.AnimatedLength ();
    r.value = "50mm";
    if (viewer.svg.root_element.width != null) {
      cx.base_val.value = viewer.svg.root_element.width.base_val.value / 2.0;
    }
    if (viewer.svg.root_element.height != null) {
      cy.base_val.value = viewer.svg.root_element.height.base_val.value / 2.0;
    }
    var text = """<circle cx="%s" cy="%s" r="%s" />""".printf (cx.value, cy.value, r.value);
    write_string (text);
  }
  public void write_ellipse () {
    GSvg.AnimatedLength cx, cy, rx, ry;
    cx = new GSvg.AnimatedLength ();
    cx.value = "0mm";
    cy = new GSvg.AnimatedLength ();
    cy.value = "0mm";
    rx = new GSvg.AnimatedLength ();
    rx.value = "50mm";
    ry = new GSvg.AnimatedLength ();
    ry.value = "50mm";
    if (viewer.svg.root_element.width != null) {
      cx.base_val.value = viewer.svg.root_element.width.base_val.value / 2.0;
      rx.base_val.value = viewer.svg.root_element.width.base_val.value / 4.0;
    }
    if (viewer.svg.root_element.height != null) {
      cy.base_val.value = viewer.svg.root_element.height.base_val.value / 2.0;
      ry.base_val.value = viewer.svg.root_element.height.base_val.value / 8.0;
    }
    var text = """<ellipse cx="%s" cy="%s" rx="%s" ry="%s" />""".printf (cx.value, cy.value, rx.value, ry.value);
    write_string (text);
  }
  public void write_line () {
    GSvg.AnimatedLength x1, y1, x2, y2;
    x1 = new GSvg.AnimatedLength ();
    x1.value = "0mm";
    y1 = new GSvg.AnimatedLength ();
    y1.value = "0mm";
    x2 = new GSvg.AnimatedLength ();
    x2.value = "50mm";
    y2 = new GSvg.AnimatedLength ();
    y2.value = "50mm";
    if (viewer.svg.root_element.width != null) {
      x1.base_val.value = viewer.svg.root_element.width.base_val.value / 4.0;
      x2.base_val.value = viewer.svg.root_element.width.base_val.value / 2.0;
    }
    if (viewer.svg.root_element.height != null) {
      y1.base_val.value = viewer.svg.root_element.height.base_val.value / 4.0;
      y2.base_val.value = viewer.svg.root_element.height.base_val.value / 2.0;
    }
    var text = """<line style="stroke: black; stroke-width: 2mm" x1="%s" y1="%s" x2="%s" y2="%s" />""".printf (x1.value, y1.value, x2.value, y2.value);
    write_string (text);
  }
  public void write_rect () {
    GSvg.AnimatedLength x, y, width, height;
    x = new GSvg.AnimatedLength ();
    x.value = "0mm";
    y = new GSvg.AnimatedLength ();
    y.value = "0mm";
    width = new GSvg.AnimatedLength ();
    width.value = "50mm";
    height = new GSvg.AnimatedLength ();
    height.value = "50mm";
    if (viewer.svg.root_element.width != null) {
      x.base_val.value = viewer.svg.root_element.width.base_val.value / 4.0;
      width.base_val.value = viewer.svg.root_element.width.base_val.value / 2.0;
    }
    if (viewer.svg.root_element.height != null) {
      y.base_val.value = viewer.svg.root_element.height.base_val.value / 4.0;
      height.base_val.value = viewer.svg.root_element.height.base_val.value / 2.0;
    }
    var text = """<rect x="%s" y="%s" width="%s" height="%s" />""".printf (x.value, y.value, width.value, height.value);
    write_string (text);
  }
  public void write_text () {
    GSvg.AnimatedLength x, y, size;
    x = new GSvg.AnimatedLength ();
    x.value = "0mm";
    y = new GSvg.AnimatedLength ();
    y.value = "0mm";
    size = new GSvg.AnimatedLength ();
    size.value = "50mm";
    if (viewer.svg.root_element.width != null) {
      x.base_val.value = viewer.svg.root_element.width.base_val.value / 4.0;
    }
    if (viewer.svg.root_element.height != null) {
      y.base_val.value = viewer.svg.root_element.height.base_val.value / 4.0;
      size.base_val.value = viewer.svg.root_element.height.base_val.value / 4.0;
    }
    var text = """<text x="%s" y="%s" style="fill: blue; font-family: Times; font-size: %s" >Text</text>""".printf (x.value, y.value, size.value);
    write_string (text);
  }
}
